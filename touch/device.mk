# Touch
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE += vendor/google/pixelparts/touch/framework_compatibility_matrix.xml
PRODUCT_PACKAGES += \
    vendor.lineage.touch@1.0-service.pixel

BOARD_SEPOLICY_DIRS += vendor/google/pixelparts/sepolicy/touch
